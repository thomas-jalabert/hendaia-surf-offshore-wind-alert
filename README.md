This script sends mail alert when the wind is offshore on the hendaia beach. This alert is very accurate beceause the datas are extracted from the meteo station of the "chateau abbadia" of Hendaye, front of the sea.
If you want to be added to the alert, send me your mail.

Ce script envoie des alertes mails quand le vent est offshore à Hendaye. Cette alerte est très précise car les données sont extraites de la station météo du chateau Abbadia d'Hendaye, face à la mer.
Si vous voulez être ajouté à l'alerte, envoyez-moi votre mail.
