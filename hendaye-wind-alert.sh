#!/bin/bash

## Variables
mails="PUT_YOUR_MAILS"
mail_from="PUT_YOUR_MAIL_FROM"
minimum_interval_mail=14400

## Pre-requis et constantes
touch last-mail-date
now=$(date +"%s")
now_human=$(date +"%F %HH%M")


## Fonctions

# Do not send if recent sending.
anti-spam() {
last_mail_date=$(cat last-mail-date)
[ -z $last_mail_date ] && return 0
echo $last_mail_date | grep -E "^[[:digit:]]+$" &>/dev/null || return 1
diff_secondes=$(( $now - $last_mail_date ))
if [ $diff_secondes -ge $minimum_interval_mail ]
then
    return 0
else	
    exit 1
fi
}


# Send mail and momorize the sending timestamp
envoi-mail() {
echo $now > last-mail-date
echo -e "2 dernières directions du vent : \n$windDirection1 degrés \n$windDirection2 degrés\n\nDonnées de la station météo du chateau Abbadia:\nhttps://www.infoclimat.fr/observations-meteo/temps-reel/hendaye-domaine-d-abbadia/STATIC0044.html" | mail -r $mail_from -s "Hendaye Offshore - $now_human" $mails
}


## Execution

# Retrieve the 2 last wind directions
windDirection=$(curl -s https://www.infoclimat.fr/observations-meteo/temps-reel/hendaye-domaine-d-abbadia/STATIC0044.html | grep -o -E "Vent de direction [[:digit:]]+&deg" | grep -o -E "[[:digit:]]+" | head -2)
windDirection1=$(echo "$windDirection" | head -1)
windDirection2=$(echo "$windDirection" | tail -1)


# If the two are offshore, controle if a mail has been sent recently. If not, send a mail
[ $windDirection1 -ge 95 -a $windDirection1 -lt 250 -a $windDirection2 -ge 95 -a $windDirection2 -lt 250 ] && anti-spam && envoi-mail
